#!/bin/bash

# strings2stringtable.sh
# Copyright (C) 2016 Roman M. Yagodin <roman.yagodin@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

STRINGS_FILE="strings.xml"
STRINGTABLE_FILE="stringtable.xml"
TMP_FILE=".~tmp.xml"
TRANSFORM="strings2stringtable.xslt"

# check for xsltproc is available
if [ ! -x "$(command -v xsltproc)" ]; then
    echo "The 'xsltproc' utility is not installed."
    exit
fi

# check for language directory is specified
if [ -z "$1" ] || [ ! -d "lang/$1" ]; then
    echo "Please specify valid language directory name!"
    exit
fi

pushd . > /dev/null
cd "lang/$1"

# perform the transformation
xsltproc --novalid -o "$TMP_FILE" "../../$TRANSFORM" "$STRINGS_FILE"

# check result
if [ $? -eq 0 ]; then
    cp -f "$TMP_FILE" "$STRINGTABLE_FILE"
    echo "The '$STRINGTABLE_FILE' was generated from '$STRINGS_FILE'."
else
    echo "Something goes wrong then generating '$STRINGTABLE_FILE' from '$STRINGS_FILE'."
fi 

rm -f "$TMP_FILE"
popd > /dev/null
