# About PlaneShift.Translation

*PlaneShift.Translation* is community experiment on using Transifex.com to empower [PlaneShift 3D fantasy MMORPG](http://www.planeshift.it) client UI translation.

Currently, `stringtable2strings.sh` shell script could be used to create Transifex-translatable `strings.xml` file from `stringtable.xml` file from *PlaneShift* source code. All previous translations from source tree were converted and uploaded to the *Transifex.com* and [available to the translation online](https://www.transifex.com/planeshift-community/planeshift-client)!

The `string.xml` files with translations could be downloaded from Transifex either manually (use *Download file to translate* link) or with [Transifex client utility](http://docs.transifex.com/client/) (see command-line example below).

To create `stringtable.xml` file (which can be used by PlaneShift client) the `strings2stringtable.sh` shell script could be used. It reads the `strings.xml` file containing translated strings and applies XSLT transformation to get `stringtable.xml` file.

## Pull translation from Transifex via command-line (*nix)

```sh
# Force pull russian translation
tx pull -l ru --mode=translator -f

# Convert strings.xml to stringtable.xml
./strings2stringtable.sh ru

```

## TODO

- [x] Script to create `strings.xml` from `stringtable.xml`.
- [x] Script to create `stringtable.xml` from translated `strings.xml` from Transifex.
- [x] Transifex client integration.
- [x] Complete russian translation (still need review).
- [ ] Complete the translations for other languages, eventually.

## Online tools

You could use one of the free online XSL transformation services (like [this one](http://xslt.online-toolz.com/tools/xslt-transformation.php)) to produce PlaneShift client compatible `stringtable.xml` from `strings.xml` file which you can get from Transifex. In order to do this:

1. Copy the content of downloaded `strings.xml` file into *XML Input* field;
2. Copy `stringtable2strings.xslt` file content into *XSL Input* field;
3. Click *Run XSLT*;
4. Create new `stringtable.xml` file from the content of *Result* field.

## Contribute

You can contribute to the translation via [PlaneShift translation project on Transifex](https://www.transifex.com/planeshift-community/planeshift-client) - the only thing you need is [free Transifex.com account](https://www.transifex.com/signin/). Feel free to join [PlaneShift Community translation team](https://www.transifex.com/planeshift-community/teams/)!

You can also contribute translations and code directly to this repository via *git* pull requests.

## Links

- [PlaneShift translation project on Transifex](https://www.transifex.com/planeshift-community/planeshift-client/)
- [Language resources folder in PlaneShift source tree](https://sourceforge.net/p/planeshift/code/HEAD/tree/trunk/lang/)
- [PS Translations page on PSwiki](http://planeshift.top-ix.org/pswiki/index.php?title=PS_Translations)
- [PlaneShift official website](http://www.planeshift.it)
- [Forum topic on PlaneShift official forums](http://207.244.96.64/PlaneShift/smf/index.php?topic=42868)

## Licenses

- The translation resources in the `lang` folder is published under the terms of [PlaneShift License](http://www.planeshift.it/License).
- Any other code (including shell and XSL scripts) is published under the terms of [GNU General Public License version 3](http://www.gnu.org/licenses/gpl-3.0.html) or (at your option) any later version.


