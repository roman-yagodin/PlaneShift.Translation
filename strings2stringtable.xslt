<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes" encoding="UTF-8" />
    <xsl:preserve-space elements="item" />
    <xsl:template match="/resources">
        <xsl:for-each select="string">
            <xsl:if test="@name='Translation.Author'">
                <Author><xsl:value-of select="text()" /></Author>
            </xsl:if>
        </xsl:for-each>
        <StringTable>
            <xsl:apply-templates />
        </StringTable> 
    </xsl:template>
    <xsl:template match="string">
        <xsl:if test="not(@name='Translation.Author')">
            <item>
                <xsl:attribute name="orig" xsl:white-space="preserve"><xsl:value-of select="@name" /></xsl:attribute>
                <xsl:attribute name="trans"><xsl:value-of select="text()" /></xsl:attribute>
            </item>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>