#!/bin/bash

# stringtable2strings.sh
# Copyright (C) 2016 Roman M. Yagodin <roman.yagodin@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

STRINGS_FILE="strings.xml"
STRINGTABLE_FILE="stringtable.xml"
TMP_FILE=".~tmp.xml"

# check for language directory is specified
if [ -z "$1" ] || [ ! -d "lang/$1" ]; then
    echo "Please specify valid language directory name!"
    exit
fi

pushd . > /dev/null
cd "lang/$1"

echo "<resources>" > "$TMP_FILE"
cat "$STRINGTABLE_FILE" >> "$TMP_FILE"

# remove xml header, if any (we add it later)
sed -i 's/^<?xml.*?>\s*$//' "$TMP_FILE"

# remove StringTable tags, if any
sed -i 's/<[S,s]tring[T,t]able>//' "$TMP_FILE"
sed -i 's/<\/[S,s]tring[T,t]able>//' "$TMP_FILE"

# replace <author> tag with translatable <string> tag
sed -i 's/<[A,a]uthor\s*text=\"\(.*\)\"\s*\/>/<string name=\"Translation.Author\">\1<\/string>/' "$TMP_FILE"

# fix duplicate <item orig="%s from %s: %s" ... /> in some source files (remove first occurence)
sed -i 's/<item\s\+orig=\"%s from %s: %s\".*\/>//' "$TMP_FILE"

# fix unescaped '&' in 'o&clock' in some source files
sed -i 's/o\&clock/o\&amp;clock/g' "$TMP_FILE"

# remove item elements with empty orig attributes in some source files
sed -i 's/<item\s\+orig=\"\s*\".*\/>//g' "$TMP_FILE"

# replace <item> tags with <string> tags
sed -i 's/<item/<string/g' "$TMP_FILE"
sed -i 's/orig=/name=/g' "$TMP_FILE"
sed -i 's/\s*trans=\"/>/g' "$TMP_FILE"
sed -i 's/\"\s*\/>/<\/string>/g' "$TMP_FILE"

echo "</resources>" >> "$TMP_FILE"

# add xml header and rest of content
echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" > "$STRINGS_FILE"
cat "$TMP_FILE" >> "$STRINGS_FILE"

rm -f "$TMP_FILE"
popd > /dev/null